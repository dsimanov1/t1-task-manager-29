package ru.t1.simanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.command.AbstractCommand;
import ru.t1.simanov.tm.enumerated.Role;

public final class TestCommand extends AbstractCommand {

    @Override
    public void execute() {
        System.out.println("TEST!");
    }

    @Override
    public String getArgument() {
        return "-t";
    }

    @Override
    public @NotNull String getDescription() {
        return "TEST COMMAND";
    }

    @Override
    public @NotNull String getName() {
        return "test";
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
